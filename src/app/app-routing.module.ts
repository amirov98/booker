import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPageComponent } from './login-page/login-page.component';
import { ListComponent } from './list/list.component';
import { RegistrationPageComponent } from './registration-page/registration-page.component';
import { AuthGuard } from './services/auth.guard';
import { AboutComponent } from './about/about.component';
import { FutureDevelopmentsComponent } from './future-developments/future-developments.component';
import { SearchComponent } from './search/search.component';


const routes: Routes = [
  { path: 'registration', component: RegistrationPageComponent, pathMatch: 'full' },
  { path: '', redirectTo: 'login', pathMatch: 'full'},
  { path: 'login',  component: LoginPageComponent },
  { path: 'events', component: ListComponent, canActivate: [AuthGuard]},
  { path: 'about', component: AboutComponent, pathMatch: 'full' },
  { path: 'future-developments', component: FutureDevelopmentsComponent, pathMatch: 'full' },
  { path: 'search', component: SearchComponent, canActivate: [AuthGuard], pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule { }
export const routingComponents = [LoginPageComponent, ListComponent];
