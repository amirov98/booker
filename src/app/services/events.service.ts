import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from './authentication.service';
import { map, take, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { EventData } from './event-data.model';

@Injectable({
  providedIn: 'root'
})
export class EventsService {
  item: any;

  constructor(
              private http: HttpClient,
              private authService: AuthenticationService) { }

  // events() {
  //   this.http.get('http://localhost:3000/api/user/events')
  //     .subscribe(response => {
  //       console.log(response);
  //     });
  // }

  signIn(): Observable<any> {
    return this.http.get<any>('http://localhost:3000/api/user/auth/google');
  }

  google() {
    this.http.get('http://localhost:3000/api/user/auth/google')
      .subscribe();
  }

  googleSuc(): Observable<any> {
    return this.http.get('http://localhost:3000/api/user/auth/google/session',
      {observe: 'response'});
      // .subscribe(response => {
      //   console.log('success', response.status);
      // });
  }

  // listEvents(): Observable<any> {
  //   let response = this.http.get<any>('http://localhost:3000/api/user/auth/calendar/events');
  //   response.toPromise().then(res => console.log("res ", res)).catch(err => console.log("err ", err));
  //   return response;
  // }

  addEvents(location: string, team: string, members: [string], startDateAndTime: Date, endDateAndTime: Date): Observable<any> {
    const eventData: EventData = { location, team, members, startDateAndTime, endDateAndTime };
    console.log(eventData);
    return this.http.post('http://localhost:3000/api/user/addEvent', eventData, {observe: 'response'});
  }

  getEvents(): Observable<any> {
    return this.http.get('http://localhost:3000/api/user/getEvents',
    {observe: 'response'});
  }

  deleteEvent(eventId: string): Observable<any> {
    return this.http.delete<any>('http://localhost:3000/api/user/deleteEvent/' + eventId,
    {observe: 'response'});
  }

  getEventsRoom1(): Observable<any> {
    return this.http.get<any>('http://localhost:3000/api/user/getEvents/room1',
    {observe: 'response'});
  }

  getEventsRoom2(): Observable<any> {
    return this.http.get<any>('http://localhost:3000/api/user/getEvents/room2',
    {observe: 'response'});
  }

  getEventsRoom3(): Observable<any> {
    return this.http.get<any>('http://localhost:3000/api/user/getEvents/room3',
    {observe: 'response'});
  }

  getEventsByName(name: string): Observable<any> {
    return this.http.get<any>('http://localhost:3000/api/user/getEvents/member/' + name,
    {observe: 'response'});
  }

  getEventsByTeam(team: string): Observable<any> {
    return this.http.get<any>('http://localhost:3000/api/user/getEvents/team/' + team,
    {observe: 'response'});
  }
}
