export interface EventData {
  location: string;
  team: string;
  members?: [string];
  startDateAndTime: Date;
  endDateAndTime: Date;
}
