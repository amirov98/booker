import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ConfirmPasswordValidator } from './confirm-password.validator';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-registration-page',
  templateUrl: './registration-page.component.html',
  styleUrls: ['./registration-page.component.scss']
})
export class RegistrationPageComponent implements OnInit {
  registrationForm: FormGroup;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthenticationService) { }

    ngOnInit() {
      this.registrationForm = this.formBuilder.group({
        email: [ '', Validators.required ],
        password: [ '', Validators.required ],
        confirmPassword: [ '', Validators.required]
      },
      { validator: ConfirmPasswordValidator.MatchPassword });
  }

  onRegister(): void {
    if (this.registrationForm.valid) {
      this.submitted = true;
      this.authService.register(this.registrationForm.value.email, this.registrationForm.value.password);
      this.router.navigate([ '/login' ]);
    }
  }

}
