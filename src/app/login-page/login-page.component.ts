import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  constructor(
              private formBuilder: FormBuilder,
              private authService: AuthenticationService) {
              }

  public loginForm = this.formBuilder.group({
    email: [ '', Validators.required ],
    password: [ '', Validators.required ],
  });

  ngOnInit() {
  }

  onLogin(): void {
    console.log('onLogin');
    if (this.loginForm.valid) {
      this.authService.login(this.loginForm.value.email, this.loginForm.value.password);
    }
  }

}
