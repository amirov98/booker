import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EventsService } from '../services/events.service';

@Component({
  selector: 'app-future-developments',
  templateUrl: './future-developments.component.html',
  styleUrls: ['./future-developments.component.scss']
})
export class FutureDevelopmentsComponent implements OnInit {
  item = '';
  isAuth = false;
  auth2: any;
  loginElement: any;

  constructor(
              private http: HttpClient,
              private eventsService: EventsService) { }

  ngOnInit() {
  }

  signIn() {
    this.eventsService.signIn()
    .subscribe(
      data => {
        this.item = data.auth;
        console.log(data.auth);
      }
    );
  }

  onDone() {
    this.eventsService.googleSuc()
    .subscribe(
      data => {
      if (data.status === 200) {
        this.isAuth = true;
      }
      console.log('isAuth ', this.isAuth);
      }
    );
  }

}
