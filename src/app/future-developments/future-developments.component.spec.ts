import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FutureDevelopmentsComponent } from './future-developments.component';

describe('FutureDevelopmentsComponent', () => {
  let component: FutureDevelopmentsComponent;
  let fixture: ComponentFixture<FutureDevelopmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FutureDevelopmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FutureDevelopmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
