import { Component, OnInit } from '@angular/core';
import { EventsService } from '../services/events.service';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { DialogComponent } from '../dialog/dialog.component';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  isEmpty = false;
  events = [];

  constructor(
              private eventsService: EventsService,
              private formBuilder: FormBuilder,
              private dialog: MatDialog) { }

  public searchForm = this.formBuilder.group({
    name: ['', Validators.required ]
  });

  public teamSearchForm = this.formBuilder.group({
    teamSearch: ['', Validators.required ]
  });

  ngOnInit() {
  }

  onSearch() {
    console.log(this.searchForm.value.name);
    this.eventsService.getEventsByName(this.searchForm.value.name).subscribe(
      res => {
        if (res.status === 200) {
          this.isEmpty = false;
          this.events = res.body.events;
          console.log('search events ', res);
        } else if (res.status === 204) {
          this.isEmpty = true;
          this.events = [];
        } else {
          this.openDialog();
        }
      }
    );
  }

  onTeamSearch() {
    const team = this.teamSearchForm.value.teamSearch;
    console.log(team);
    console.log(typeof(team));
    this.eventsService.getEventsByTeam(team.toUpperCase()).subscribe(
      res => {
        if (res.status === 200) {
          this.isEmpty = false;
          this.events = res.body.events;
        } else if (res.status === 204) {
          this.isEmpty = true;
          this.events = [];
        } else {
          this.openDialog();
        }
      }
    );
  }

  openDialog() {
    this.dialog.open(DialogComponent);
  }

  onRoom1() {
    this.eventsService.getEventsRoom1().subscribe(res => {
      if (res.status === 200) {
        this.isEmpty = false;
        this.events = res.body.events;
      } else if (res.status === 204) {
        this.isEmpty = true;
        this.events = [];
      } else {
        this.openDialog();
      }
      // this.onGetEvents();
      console.log('room1 events ', this.events);
    });
  }

  onRoom2() {
    this.eventsService.getEventsRoom2().subscribe(res => {
      if (res.status === 200) {
        this.isEmpty = false;
        this.events = res.body.events;
      } else if (res.status === 204) {
        this.isEmpty = true;
        this.events = [];
      } else {
        this.openDialog();
      }
      // this.onGetEvents();
      console.log('room1 events ', this.events);
    });
  }

  onRoom3() {
    this.eventsService.getEventsRoom3().subscribe(res => {
      if (res.status === 200) {
        this.isEmpty = false;
        this.events = res.body.events;
      } else if (res.status === 204) {
        this.isEmpty = true;
        this.events = [];
      } else {
        this.openDialog();
      }
      // this.onGetEvents();
      console.log('room1 events ', this.events);
    });
  }

  onDelete(id) {
    console.log(id);
    if (this.events.length > 0) {
      this.eventsService.deleteEvent(id).subscribe(res => {
        if (res.status === 200) {
          this.onGetEvents();
        } else {
          this.openDialog();
        }
    });
    // TODO refresh on status 200
    } else {
      this.openDialog();
    }
  }

  onGetEvents() {
    // let eventList;
    console.log('on get events');
    this.eventsService.getEvents()
      .subscribe(res => {
        // TODO: HANDLE STATUS OF THE RESPONSE (if 204 err else res)
        if (res.status === 200) {
          // eventList = res;
          this.isEmpty = false;
          this.events = res.body.events;
          // console.log('events are ', this.events[0]._id);
        } else if (res.status === 204) {
          this.isEmpty = true;
          this.events = [];
        } else {
          this.openDialog();
        }
        // TODO: events = eventsList
      });
  }

}
