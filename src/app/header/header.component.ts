import { Component, OnInit, OnDestroy } from '@angular/core';
import { EventsService } from '../services/events.service';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  userIsAuthenticated = false;
  private authListenerSubs: Subscription;

  constructor(
              private authService: AuthenticationService,
              private router: Router
              ) { }

  ngOnInit() {
    this.userIsAuthenticated = this.authService.getIsAuth();
    this.authListenerSubs = this.authService.
      getAuthStatusListener()
      .subscribe(isAuthenticated => {
        this.userIsAuthenticated = isAuthenticated;
      });
  }

  ngOnDestroy() {
    this.authListenerSubs.unsubscribe();
  }

  onRegister() {
    this.router.navigate(['registration']);
  }

  onLogout() {
    this.authService.logout();
  }

  onFuture() {
    this.router.navigate(['future-developments']);
  }

  onAbout() {
    this.router.navigate(['about']);
  }

  onLogin() {
    this.router.navigate(['login']);
  }

  onSearch() {
    this.router.navigate(['search']);
  }

  onEvents() {
    this.router.navigate(['events']);
  }

}
