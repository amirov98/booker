import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DialogComponent } from '../dialog/dialog.component';
import { EventsService } from '../services/events.service';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  // item = '';
  // isAuth = false;
  isEmpty = false;
  events = [];
  selected = '';

  constructor(public dialog: MatDialog,
              public eventsService: EventsService,
              private formBuilder: FormBuilder) {
            }


  public eventAdditionForm = this.formBuilder.group({
    location: [ '', Validators.required ],
    team: [ '' ],
    members: [ [] ],
    startDateAndTime: [ '' , Validators.required ],
    endDateAndTime: [ '' , Validators.required ]
  });

  ngOnInit() {
    this.onGetEvents();
  }

  onAdd() {
    const startDate = new Date(this.eventAdditionForm.value.startDateAndTime);
    const endDate = new Date(this.eventAdditionForm.value.endDateAndTime);
    if (endDate > startDate ) {
      this.eventsService.addEvents(
        this.eventAdditionForm.value.location,
        this.eventAdditionForm.value.team = this.selected,
        this.eventAdditionForm.value.members.length > 0
          ? this.eventAdditionForm.value.members.split(' ')
          : [''],
        this.eventAdditionForm.value.startDateAndTime.replace(' ', 'T'),
        this.eventAdditionForm.value.endDateAndTime).subscribe((res) => {
          if (res.status === 200) {
            this.onGetEvents();
          } else {
            this.openDialog();
          }
        });
    } else {
      this.openDialog();
    }
  }

  onGetEvents() {
    // let eventList;
    console.log('on get events');
    this.eventsService.getEvents()
      .subscribe(res => {
        // TODO: HANDLE STATUS OF THE RESPONSE (if 204 err else res)
        if (res.status === 200) {
          // eventList = res;
          this.isEmpty = false;
          this.events = res.body.events;
          this.events.forEach(event => {
            event.startDateAndTime = new Date(event.startDateAndTime).toLocaleString();
          });
          this.events.forEach(event => {
            event.endDateAndTime = new Date(event.endDateAndTime).toLocaleString();
          });
          // console.log('events are ', this.events[0]._id);
        } else if (res.status === 204) {
          this.isEmpty = true;
          this.events = [];
        } else {
          this.openDialog();
        }
        // TODO: events = eventsList
      });
  }

  onDelete(id) {
    console.log(id);
    if (this.events.length > 0) {
      this.eventsService.deleteEvent(id).subscribe(res => {
        if (res.status === 200) {
          this.onGetEvents();
        } else {
          this.openDialog();
        }
    });
    // TODO refresh on status 200
    } else {
      this.openDialog();
    }
  }

  openDialog() {
    this.dialog.open(DialogComponent);
  }
}
